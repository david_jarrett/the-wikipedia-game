package edu.cs3152.wikipedia.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a vertex in a directed graph.
 * 
 * @author David Jarrett & Ras Fincher
 * @version Fall 2018
 */
public class Vertex {

	private String id;
	private String name;
	private List<Vertex> neighbors = new ArrayList<>();
	private Vertex previousNode;

	/**
	 * Initialize the vertex.
	 * 
	 * @param id   The vertex id.
	 * @param name A name/description.
	 */
	public Vertex(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	/**
	 * Adds the specified vertex to this vertex's adjacency list.
	 * 
	 * @param neighbor The vertex to add to the adjacency list.
	 */
	public void addNeighbor(Vertex neighbor) {
		this.neighbors.add(neighbor);
	}

	public List<Vertex> getNeighbors() {
		return this.neighbors;
	}

	/**
	 * A temporary pointer to a previous vertex.
	 * 
	 * @param previousVertex The vertex to be considered previous to this vertex.
	 */
	public void setPreviousVertex(Vertex previousVertex) {
		this.previousNode = previousVertex;
	}

	public Vertex getPreviousNode() {
		return this.previousNode;
	}

}
