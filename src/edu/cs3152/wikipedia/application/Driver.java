package edu.cs3152.wikipedia.application;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import edu.cs3152.wikipedia.model.WikipediaGraph;

/**
 * Application entry point.
 * 
 * @author David Jarrett & Ras Fincher
 * @version Fall 2018
 */
public class Driver {

	public static void main(String[] args) {
		try {
			System.out.println("Please wait for initialization. It should take less than one minute.");
			WikipediaGraph graph = new WikipediaGraph("WikipediaIDs.txt", "WikipediaLinks.txt");
			System.out.println("Initialization complete.");

			System.out.println(System.lineSeparator());
			System.out.println("At the prompt, enter article names to search. If you would like to see the top 100");
			System.out.println("linked-to articles, type 100. You could also just press ENTER to quit.");

			beginApplication(graph);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void beginApplication(WikipediaGraph graph) {
		Scanner scan = new Scanner(System.in);
		for (;;) {
			System.out.println(System.lineSeparator());
			System.out.print("Enter source article or 100: ");
			String source = scan.nextLine();

			if (source.equals("")) {
				System.out.println("Goodbye");
				return;
			} else if (source.equals("100")) {
				doTop100Search(graph);
			} else {
				doSearch(graph, scan, source);
			}
		}
	}

	private static void doSearch(WikipediaGraph graph, Scanner scan, String source) {
		System.out.print("Enter target article: ");
		String target = scan.nextLine();

		List<String> result = graph.getShortestPath(source, target);
		if (result == null) {
			System.out.println("The specified article(s) do not exist, or a path does not exist between them.");
		} else {
			printResult(result);
		}
	}

	private static void printResult(List<String> result) {
		System.out.println();
		System.out.println("Path:");
		System.out.println("--------------");
		for (var vertex : result) {
			System.out.println(vertex);
		}
		System.out.println("--------------");
		System.out.println("Link distance: " + (result.size() - 1));
	}

	private static void doTop100Search(WikipediaGraph graph) {
		System.out.println("\nCollecting results, this should only take a minute...");
		List<String> result = graph.getMostLinkedPages(100);
		for (var r : result) {
			System.out.println(r);
		}
	}

}
