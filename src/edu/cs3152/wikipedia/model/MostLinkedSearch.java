package edu.cs3152.wikipedia.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Represents a search for the top n linked-to articles.
 * 
 * @author David Jarrett & Ras Fincher
 * @version Fall 2018
 */
public class MostLinkedSearch {

	private Map<String, Vertex> vertices;

	/**
	 * Initialize the search with the vertices to be searched.
	 * 
	 * @param vertices The vertices to be searched.
	 */
	public MostLinkedSearch(Map<String, Vertex> vertices) {
		this.vertices = vertices;
	}

	/**
	 * Determine the n most linked-to articles.
	 * 
	 * @param resultQuantity The number of results to find.
	 * @return A list of results, sorted in most-linked to order.
	 */
	public List<String> performSearch(int resultQuantity) {
		Map<Vertex, Integer> counts = new HashMap<>();
		for (Vertex v : this.vertices.values()) {
			for (Vertex neighbor : v.getNeighbors()) {
				int vertexCount = counts.getOrDefault(neighbor, 0);
				counts.put(neighbor, vertexCount + 1);
			}
		}

		PriorityQueue<VertexCountCombo> sorter = new PriorityQueue<>();
		for (Vertex v : counts.keySet()) {
			VertexCountCombo combo = new VertexCountCombo(v, counts.get(v));
			sorter.add(combo);
		}

		List<String> result = new ArrayList<>();
		for (int i = 0; i < resultQuantity; i++) {
			VertexCountCombo nextCombo = sorter.poll();
			Vertex nextVertex = nextCombo.getVertex();
			String resultString = nextVertex.getName() + ": " + nextCombo.getCount() + " links.";
			result.add(resultString);
		}

		return result;
	}

	private class VertexCountCombo implements Comparable<VertexCountCombo> {

		private Vertex vertex;
		private int count;

		public VertexCountCombo(Vertex vertex, int count) {
			this.vertex = vertex;
			this.count = count;
		}

		public Vertex getVertex() {
			return this.vertex;
		}

		public int getCount() {
			return this.count;
		}

		@Override
		public int compareTo(VertexCountCombo o) {
			return o.count - this.count;
		}

	}

}
