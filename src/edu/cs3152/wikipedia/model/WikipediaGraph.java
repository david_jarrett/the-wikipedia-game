package edu.cs3152.wikipedia.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * Representation of a directed graph.
 * 
 * @author David Jarrett & Ras Fincher
 * @version Fall 2018
 */
public class WikipediaGraph {

	private Map<String, String> ids = new HashMap<>(10_000_000);
	private Map<String, Vertex> vertices = new HashMap<>(10_000_000);

	/**
	 * Initialize the graph.
	 * 
	 * @param idFile   The file containing the mappings from article title to
	 *                 article ID.
	 * @param linkFile The file containing the articles linked-to by other articles.
	 * @throws FileNotFoundException If either file cannot be found.
	 * @throws IOException           If something horrible happens.
	 */
	public WikipediaGraph(String idFile, String linkFile) throws FileNotFoundException, IOException {
		loadIDsAndCreateVertices(idFile);
		setNeighbors(linkFile);
	}

	private void loadIDsAndCreateVertices(String idFile) throws FileNotFoundException, IOException {
		var bf = new BufferedReader(new FileReader(idFile));
		String line;
		while ((line = bf.readLine()) != null) {
			var parts = line.split(":");
			this.ids.put(parts[0], parts[1]);
			this.vertices.put(parts[1], new Vertex(parts[1], parts[0]));
		}
		bf.close();
	}

	private void setNeighbors(String linkFile) throws FileNotFoundException, IOException {
		var bf = new BufferedReader(new FileReader(linkFile));
		String line;
		while ((line = bf.readLine()) != null) {
			setNeighborsFrom(line);
		}
		bf.close();
	}

	private void setNeighborsFrom(String line) {
		var parts = line.split(" ");
		Vertex currentVertex = this.vertices.get(parts[0]);
		for (var i = 1; i < parts.length; i++) {
			Vertex neighborVertex = this.vertices.get(parts[i]);
			currentVertex.addNeighbor(neighborVertex);
		}
	}

	/**
	 * Determines the shortest path between two vertices.
	 * 
	 * @param from The source vertex.
	 * @param to   The target vertex.
	 * @return Returns a list of vertices corresponding to the shortest path between
	 *         the specified vertices.
	 */
	public List<String> getShortestPath(String from, String to) {
		String sourceID = this.ids.get(from);
		String targetID = this.ids.get(to);

		if (sourceID == null || targetID == null) {
			return null;
		}

		Vertex source = this.vertices.get(sourceID);
		Vertex target = this.vertices.get(targetID);
		return breadthFirstSearch(source, target);
	}

	private List<String> breadthFirstSearch(Vertex source, Vertex target) {
		List<String> result = null;
		Queue<Vertex> queue = new LinkedList<>();
		Set<Vertex> visited = new HashSet<>();

		visited.add(source);
		queue.add(source);

		breadthFirstSearch: while (!queue.isEmpty()) {
			Vertex current = queue.remove();
			List<Vertex> neighbors = current.getNeighbors();
			for (var neighbor : neighbors) {
				if (visited.contains(neighbor)) {
					continue;
				}
				visited.add(neighbor);
				queue.add(neighbor);
				neighbor.setPreviousVertex(current);
				if (neighbor == target) {
					result = buildResultPath(neighbor);
					break breadthFirstSearch;
				}
			}
		}

		clearPreviousPointers(visited);
		return result;
	}

	private void clearPreviousPointers(Set<Vertex> visited) {
		for (Vertex v : visited) {
			v.setPreviousVertex(null);
		}
	}

	private List<String> buildResultPath(Vertex neighbor) {
		List<String> result = new ArrayList<>();
		var current = neighbor;

		while (current != null) {
			String currentVertexTitle = current.getName();
			result.add(currentVertexTitle);

			current = current.getPreviousNode();
		}

		Collections.reverse(result);
		return result;
	}

	/**
	 * Determines the n most linked-to articles.
	 * 
	 * @param pageCount The number of results to obtain.
	 * @return A list of the top n linked-to articles, sorted by most linked-to.
	 */
	public List<String> getMostLinkedPages(int pageCount) {
		return new MostLinkedSearch(this.vertices).performSearch(pageCount);
	}

}
